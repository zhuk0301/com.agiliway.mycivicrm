CREATE TABLE IF NOT EXISTS `civicrm_site_info` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Unique Identifier',
  `membership_id` INT(10) UNSIGNED NOT NULL COMMENT 'FK to Membership ID',
  `contribution_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'FK to contribution table.',
  `package_name` VARCHAR(255) NOT NULL COMMENT 'Package Name',
  `company_name` VARCHAR(255) NOT NULL COMMENT 'Company Name',
  `company_email` VARCHAR(255) NOT NULL COMMENT 'Company Email',
  `website_domain` VARCHAR(255) NOT NULL COMMENT 'Website Domain',
  `admin_login` VARCHAR(255) NOT NULL COMMENT 'Admin Login',
  `admin_password` VARCHAR(255) NOT NULL COMMENT 'Admin Password',
  `company_logo` TEXT NULL DEFAULT NULL COMMENT 'Company Logo',
  `site_status` VARCHAR(255) DEFAULT NULL COMMENT 'Site Status',
  `site_server_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'FK to site_server table.',
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `civicrm_site_server` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Unique Identifier',
  `server_id` VARCHAR(255) NOT NULL COMMENT 'Server ID',
  `server_ip` VARCHAR(255) DEFAULT NULL COMMENT 'Server IP',
  `server_status` VARCHAR(255) DEFAULT NULL COMMENT 'Server Status',
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `civicrm_handle_site` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `site_info_id` INT(10) UNSIGNED NOT NULL COMMENT 'FK to site_info table.',
  `status` VARCHAR(255) NULL DEFAULT NULL,
  `counter` INT(10) NOT NULL DEFAULT 0,
  `data` TEXT NULL DEFAULT NULL,
  `message` TEXT NULL DEFAULT NULL,
  `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
