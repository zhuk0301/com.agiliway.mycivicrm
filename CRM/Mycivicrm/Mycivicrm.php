<?php

/**
 * Class CRM_Mycivicrm_Mycivicrm
 */
class CRM_Mycivicrm_Mycivicrm {

  /**
   * @var bool
   */
  private static $isMembershipEdit = FALSE;

  /**
   * @var bool
   */
  private static $isMembershipCreate = FALSE;

  /**
   * @param $op
   * @param $objectName
   * @param $id
   * @param $params
   *
   * @throws \CRM_Core_Exception
   */
  public static function pre($op, $objectName, $id, &$params) {
    if ($objectName == 'Membership' && $op == 'create') {
      self::$isMembershipCreate = TRUE;
    }

    if ($objectName == 'Membership'
      && $op == 'edit'
      && CRM_Utils_Request::retrieve('q', 'String') == 'civicrm/contribute/transact'
      && !self::$isMembershipCreate
    ) {
      self::$isMembershipEdit = TRUE;
      $params = ['id' => $params['id']];
    }
  }

  /**
   * @param $formName
   * @param $form
   *
   * @throws \CRM_Core_Exception
   * @throws \CiviCRM_API3_Exception
   */
  public static function postProcess($formName, $form) {
    if ($formName != 'CRM_Contribute_Form_Contribution_Confirm' || !self::$isMembershipEdit || self::$isMembershipCreate) {
      return;
    }
    /* Create a new membership record and modify membership payment instead of the default renewal process.
    It is called when there is more than one membership record of the same type. */
    $params = $form->getVar('_params');
    $contributionId = $form->getVar('_contributionID');
    $membershipPaymentId = (int) civicrm_api3('MembershipPayment', 'getvalue', [
      'return' => 'id',
      'membership_id' => $params['membershipID'],
      'contribution_id' => $contributionId,
    ]);
    $memParams = [
      'contact_id' => $form->getVar('_membershipContactID'),
      'membership_type_id' => $form->getVar('_membershipBlock')['membership_type_default'],
      'source' => $params['description'],
      'status_id' => (int) civicrm_api3('MembershipStatus', 'getvalue', ['return' => 'id', 'name' => 'Pending',]),
      'is_pay_later' => $params['is_pay_later'],
      'skipStatusCal' => 1,
    ];

    if ($form->getVar('_values')['is_recur']) {
      $memParams['contribution_recur_id'] = $params['contributionRecurID'];
    }

    $membership = CRM_Member_BAO_Membership::create($memParams, $ids = [], TRUE);
    civicrm_api3('MembershipPayment', 'create', [
      'membership_id' => $membership->id,
      'contribution_id' => $contributionId,
      'id' => $membershipPaymentId,
    ]);

    if ($params['amount'] == '0') {
      civicrm_api3('Contribution', 'completetransaction', [
        'id' => $contributionId,
      ]);
    }
  }

  /**
   * @param $formName
   * @param $fields
   * @param $files
   * @param $form
   * @param $errors
   * @throws CiviCRM_API3_Exception
   */
  public static function processValidateForm($formName, &$fields, &$files, &$form, &$errors) {
    if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
      $membershipType = civicrm_api3('MembershipType', 'getsingle', [
        'return' => ["name"],
        'id' => $form->getVar('_membershipBlock')['membership_type_default'],
      ]);

      if ($membershipType['name'] != 'Custom') {
        if (!CRM_Mycivicrm_Utils_Validation::validateSubdomain($fields['website_domain']) ||
          !empty(CRM_Mycivicrm_BAO_SiteInfo::getAll(['website_domain' => $fields['website_domain'],])[0])) {
          $errors['website_domain'] = ts('The subdomain \'%1\' is not available', [1 => $fields['website_domain']]);
        }
        if (strtolower($fields['website_domain']) != $fields['website_domain']) {
          $errors['website_domain'] = ts('The subdomain \'%1\' can not contain capital letters', [1 => $fields['website_domain']]);
        }
        if (!CRM_Mycivicrm_Utils_Validation::email($fields['company_email'])) {
          $errors['company_email'] = ts('Please enter a valid company email address.');
        }
        if ($fields['admin_login'] == 'admin') {
          $errors['admin_login'] = ts('The \'%1\' login is not available', [1 => $fields['admin_login']]);
        }
      }

      if ($membershipType['name'] == 'Trial') {
        if (!empty($form->getVar('_membershipContactID'))) {
          if (CRM_Mycivicrm_Package::hasAlreadyTrial(NULL, $form->getVar('_membershipContactID'))) {
            $errors['email-Primary'] = 'This contact already used trial period.';
          }
        } else {
          $contact = civicrm_api3('Contact', 'get', [
            'email' => $fields['email-Primary'],
          ]);

          if ($contact['count'] == 1 && CRM_Mycivicrm_Package::hasAlreadyTrial(NULL, $contact['id'])) {
            $errors['email-Primary'] = 'This email already registered and used trial period.';
          }
        }
      } elseif ($membershipType['name'] == 'Custom') {
        $form->setElementError('_qf_default', NULL);
      }
    }
  }

}
