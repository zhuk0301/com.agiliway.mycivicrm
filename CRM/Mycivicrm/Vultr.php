<?php

require_once  __DIR__ . '/../../packages/Vultr.php';

class CRM_Mycivicrm_Vultr {

  /**
   * @var \Vultr
   */
  public $vultr;

  /**
   * CRM_Mycivicrm_Vultr constructor.
   */
  public function __construct() {
    $this->vultr = new Vultr(Civi::settings()->get('vultr_api_key'));
  }

  /**
   * @return array
   */
  public function getCreateServerParams() {
    return [
      'region' => Civi::settings()->get('vultr_region_id'),
      'plan' => Civi::settings()->get('vultr_plan_id'),
      'snapshot_id' => Civi::settings()->get('vultr_snapshot_id'),
      'label' => Civi::settings()->get('vultr_label'),
      //'os_id' => (int) Civi::settings()->get('vultr_os_id'),
    ];
  }
}
