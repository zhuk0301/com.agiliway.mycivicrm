<?php

class CRM_Mycivicrm_Utils_Validation extends CRM_Utils_Rule {

  /**
   * List of reserved subdomains
   *
   * @var array
   */
  protected static array $reservedSubdomains = [
    'wp', 'wordpress', 'drupal', 'joomla', 'mycivicrm', 'my', 'civicrm', 'crm', 'civi'
  ];

  public static function validateSubdomain($value): bool {
    if (!preg_match('/^[A-Za-z0-9](?:[A-Za-z0-9\-]{0,61}[A-Za-z0-9])?$/', $value)) {
      return FALSE;
    }

    $reservedSubdomains = array_merge(
      explode("\n", file_get_contents('https://raw.githubusercontent.com/ckrailo/Banned-Subdomains/master/list.txt')),
      explode("\n", file_get_contents('https://raw.githubusercontent.com/ckrailo/Banned-Subdomains/master/adult.txt')),
      static::$reservedSubdomains
    );

    if (in_array($value, $reservedSubdomains)) {
      return FALSE;
    }

    return TRUE;
  }
}