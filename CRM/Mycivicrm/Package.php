<?php

class CRM_Mycivicrm_Package {

  const TRIAL = 'Trial';
  const BASIC = 'Basic';
  const ADVANCED = 'Advanced';
  const CUSTOM = 'Custom';

  public static function hasAlreadyTrial($membershipId, $contactId = 0) {
    $hasTrial = 0;
    try {
      if (!$contactId) {
        $membership = civicrm_api3('Membership', 'getsingle', [
          'id' => $membershipId,
        ]);
        $contactId = $membership['contact_id'];
      }
      $hasTrial = (int) civicrm_api3('Membership', 'getcount', [
        'membership_type_id' => self::TRIAL,
        'contact_id' => $contactId,
      ]);
    } catch (CiviCRM_API3_Exception $e) {
      return (bool) $hasTrial;
    }
    return $hasTrial > 1;
  }

  /**
   * @param $contributionId
   *
   * @return bool
   */
  public static function checkIsContributionCompleted($contributionId) {
    try {
      $contributionStatus = civicrm_api3('Contribution', 'getvalue', [
        'return' => 'contribution_status',
        'id' => $contributionId,
      ]);
    } catch (CiviCRM_API3_Exception $e) {
      return FALSE;
    }

    return $contributionStatus == 'Completed';
  }

  /**
   * @param $contributionId
   *
   * @return array
   */
  public static function resolveMembership($contributionId) {
    try {
      $membershipId = civicrm_api3('MembershipPayment', 'getvalue', [
        'return' => 'membership_id',
        'contribution_id' => $contributionId,
      ]);
      $membership = civicrm_api3('Membership', 'getsingle', [
        'id' => $membershipId,
      ]);
    } catch (CiviCRM_API3_Exception $e) {
      return [];
    }

    return $membership;
  }

  /**
   * @param $params
   */
  public static function insertHandleSite($params): void {
    $query = 'INSERT INTO civicrm_handle_site (site_info_id, status) VALUES (%1, %2)';
    $queryParams = [
      1 => [$params['site_info_id'], 'String'],
      2 => [$params['status'], 'String'],
    ];
    CRM_Core_DAO::executeQuery($query, $queryParams);
  }

  public static function updateHandleSiteStatus($id, $status): void {
    $query = 'UPDATE civicrm_handle_site set status = %1 where id = %2';
    $queryParams = [
      1 => [$status, 'String'],
      2 => [$id, 'Int'],
    ];
    CRM_Core_DAO::executeQuery($query, $queryParams);
  }

  /**
   * @return array
   */
  public static function getHandleSiteByStatus($status): array {
    $query = 'SELECT si.id AS site_info_id, hs.id as handle_site_id
      FROM civicrm_handle_site hs
      LEFT JOIN civicrm_site_info si ON hs.site_info_id = si.id
      WHERE hs.`status` = %1 AND si.site_status != %1';
    $queryParams = [
      1 => [$status, 'String'],
    ];
    return CRM_Core_DAO::executeQuery($query, $queryParams)->fetchAll();
  }

  public static function getHandleSiteBySiteInfoId($id): array {
    $query = 'SELECT si.id AS site_info_id, hs.id as handle_site_id
      FROM civicrm_handle_site hs
      LEFT JOIN civicrm_site_info si ON hs.site_info_id = si.id
      WHERE hs.site_info_id = %1';
    $queryParams = [
      1 => [$id, 'Int'],
    ];
    return CRM_Core_DAO::executeQuery($query, $queryParams)->fetchAll();
  }

}
