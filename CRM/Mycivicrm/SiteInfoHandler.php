<?php

class CRM_Mycivicrm_SiteInfoHandler {

  /**
   * @param $formName
   * @param \CRM_Core_form $form
   *
   * @throws \CRM_Core_Exception
   */
  public static function preProcess($formName, CRM_Core_form &$form) {
    if ($formName == 'CRM_Member_Form_MembershipView') {
      $membershipId = CRM_Utils_Request::retrieve('id', 'Integer', CRM_Core_DAO::$_nullObject, FALSE, -1);
      $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['membership_id' => $membershipId]);
      $viewCustomData = $form->get_template_vars('viewCustomData');

      if (empty($siteInfo[0])) {
        return;
      }
      else {
        $siteInfo = $siteInfo[0];
      }

      // TODO: maybe redo site info fields
      foreach (self::getSiteInfoFields() as $name => $field) {
        $viewCustomData['site_info']['site_info']['fields'][$name] = [
          'field_title' => $field['title'],
          'field_type' => $field['type'],
          'field_value' => $siteInfo[$name],
        ];

        if ($field['type'] == 'File' && !empty($siteInfo[$name])) {
          $viewCustomData['site_info']['site_info']['fields'][$name]['field_value'] = '<img src="' . $siteInfo[$name] . '" style="max-width: 300px;"/>';
        }
      }

      $form->assign('viewCustomData', $viewCustomData);
    }

    if ($formName == 'CRM_Member_Form_Membership' && $form->getAction() == CRM_Core_Action::UPDATE) {
      // TODO: maybe redo it
      return;
      foreach (self::getSiteInfoFields() as $name => $field) {
        $form->add($field['type'], $name, $field['title'], '', $field['required']);

        if ($field['type'] == 'File') {
          $form->addUploadElement($name);
        }
      }
    }
  }

  /**
   * @param $formName
   * @param \CRM_Core_form $form
   *
   * @throws \CRM_Core_Exception
   */
  public static function buildForm($formName, CRM_Core_form &$form) {
    //Fix for 'Custom' membership type.
    if (!empty($form->getVar('_membershipTypeValues')) && array_values($form->getVar('_membershipTypeValues'))[0]['name'] == 'Custom') {
      return;
    }

    if (is_a($form, 'CRM_Contribute_Form_ContributionBase')) {
      $values = $form->getVar('_values');

      if (!empty($values['custom_pre_id'])) {
        try {
          $customPreTitle = civicrm_api3('UFGroup', 'getvalue', [
            'return' => 'title',
            'id' => $values['custom_pre_id'],
          ]);
        } catch (CiviCRM_API3_Exception $e) {
        }

        if (!empty($customPreTitle)) {
          $defaultValues = $form->getVar('_defaultValues');
          $siteInfoParams = self::resolveSiteInfoParams($form->getVar('_params'));

          $customPreFields = $form->get_template_vars('customPre');
          $viewOnlyFileValues = $form->get_template_vars('viewOnlyFileValues');

          foreach (self::getSiteInfoFields() as $name => $field) {
            if ($field['is_view']) {
              continue;
            }

            $element = $form->add($field['type'], $name, $field['title'], '', $field['required']);

            $customPreFields[$name] = [
              'name' => $name,
              'groupTitle' => $customPreTitle,
            ];

            if (!empty($siteInfoParams[$name])) {
              $element->setValue($siteInfoParams[$name]);
            }

            if ($field['type'] == 'File') {
              $form->addUploadElement($name);
              $customPreFields[$name]['html_type'] = $field['type'];

              if ($formName != 'CRM_Contribute_Form_Contribution_Main') {
                $path = $siteInfoParams[$name]['name'] ?? NULL;
                $fileType = $siteInfoParams[$name]['type'] ?? NULL;
                $viewOnlyFileValues[$name] = CRM_Utils_File::getFileURL($path, $fileType);
              }
            }
          }

          $form->assign('customPre', $customPreFields);
          $form->assign('viewOnlyFileValues', $viewOnlyFileValues);

          $form->setVar('_defaultValues', array_merge($defaultValues, $siteInfoParams));
        }
      }
    }

    if ($formName == 'CRM_Custom_Form_CustomDataByType') {
      // TODO: check if it is needed
      return;
      $type = CRM_Utils_Request::retrieve('type', 'String');

      if ($type == 'Membership') {
        $membershipId = CRM_Utils_Request::retrieve('entityID', 'Integer', CRM_Core_DAO::$_nullObject, FALSE, -1);
        $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['membership_id' => $membershipId]);
        $groupTree = $form->get_template_vars('groupTree');

        if (!empty($siteInfo[0])) {
          $siteInfo = $siteInfo[0];
        }

        foreach (self::getSiteInfoFields() as $name => $field) {
          $element = $form->add($field['type'], $name, $field['title'], '', $field['required']);

          $groupTree['site_info']['fields'][$name] = [
            'name' => $name,
            'label' => $field['title'],
            'data_type' => $field['data_type'],
            'html_type' => $field['type'],
            'in_selector' => 0,
            'element_name' => $name,
          ];

          if (!empty($siteInfo[$name])) {
            $element->setValue($siteInfo[$name]);
            $groupTree['site_info']['fields'][$name]['element_value'] = $siteInfo[$name];
          }

          if ($field['type'] == 'File') {
            // delete file
          }
        }

        $form->assign('groupTree', $groupTree);
      }
    }
  }

  /**
   * @param $formName
   * @param \CRM_Core_form $form
   */
  public static function postProcess($formName, CRM_Core_form $form) {
    if ($formName == 'CRM_Contribute_Form_Contribution_Confirm') {
      /* Add an entry to "site info" to handle setting up a new site */
      $membership = CRM_Mycivicrm_Package::resolveMembership($form->getVar('_contributionID'));
      $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['membership_id' => $membership['id']]);

      if (empty($siteInfo[0])) {
        $siteInfoParams = self::resolveSiteInfoParams($form->getVar('_params'));
        $siteInfoParams['membership_id'] = $membership['id'];
        $siteInfoParams['contribution_id'] = $form->getVar('_contributionID');
        $siteInfoParams['package_name'] = $membership['membership_name'];
        $siteInfoParams['site_status'] = 'process_new';

        if (!empty($siteInfoParams['company_logo']['name'])) {
          $siteInfoParams['company_logo'] = dirname(CRM_Core_Config::singleton()->extensionsURL) . '/custom/' . basename($siteInfoParams['company_logo']['name']);
        }

        CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
      }
    }

    // TODO: check if below code is needed
    return;

    if ($formName == 'CRM_Member_Form_Membership' && $form->getAction() == CRM_Core_Action::UPDATE) {
      $siteInfoParams = self::resolveSiteInfoParams($form->getVar('_params'));
      $siteInfoParams['membership_id'] = $form->getVar('_id');
      $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['membership_id' => $form->getVar('_id')]);

      if (!empty($siteInfo[0]['id'])) {
        $siteInfoParams['id'] = $siteInfo[0]['id'];
      }

      if (!empty($siteInfoParams['company_logo']['name'])) {
        $fileObj = CRM_Core_BAO_File::create([
          'mime_type' => $siteInfoParams['company_logo']['type'],
          'uri' => basename($siteInfoParams['company_logo']['name']),
          'upload_date' => date('Ymd'),
        ]);
        $siteInfoParams['company_logo'] = $fileObj->id;
      }
      elseif (!empty($siteInfo[0]['company_logo']) && !empty($siteInfo[0]['id'])) {
        $entityFileDAO = new CRM_Core_DAO_EntityFile();
        $entityFileDAO->entity_table = CRM_Mycivicrm_BAO_SiteInfo::getTableName();
        $entityFileDAO->entity_id = $siteInfo[0]['id'];
        $entityFileDAO->file_id = $siteInfo[0]['company_logo'];

        if (!$entityFileDAO->find(TRUE)) {
          $siteInfoParams['company_logo'] = 0;
        }
      }

      $siteInfoObj = CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);

      if (!empty($siteInfoParams['company_logo'])) {
        $entityFileDAO = new CRM_Core_DAO_EntityFile();
        $entityFileDAO->entity_table = CRM_Mycivicrm_BAO_SiteInfo::getTableName();
        $entityFileDAO->entity_id = $siteInfoObj->id;
        $entityFileDAO->file_id = $fileObj->id;
        $entityFileDAO->save();
      }
    }

    if ($formName == 'CRM_Member_Form_Membership' && $form->getAction() == CRM_Core_Action::DELETE) {
      $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['membership_id' => $form->getVar('_id')]);

      if (!empty($siteInfo[0]['id'])) {
        if (!empty($siteInfo[0]['company_logo'])) {
          CRM_Core_BAO_File::deleteEntityFile(CRM_Mycivicrm_BAO_SiteInfo::getTableName(), $siteInfo[0]['id'], NULL, $siteInfo[0]['company_logo']);
        }

        CRM_Mycivicrm_BAO_SiteInfo::del($siteInfo[0]['id']);
      }
    }
  }

  /**
   * @return array
   */
  public static function getSiteInfoFields() {
    // TODO: maybe redo site info fields
    $siteInfoFields = [
      'company_name' => [
        'type' => 'text',
        'title' => ts('Company Name'),
        'required' => TRUE,
        'data_type' => 'String',
        'is_view' => 0,
      ],
      'company_email' => [
        'type' => 'text',
        'title' => ts('Company Email'),
        'required' => TRUE,
        'data_type' => 'String',
        'is_view' => 0,
      ],
      'website_domain' => [
        'type' => 'text',
        'title' => ts('Website Domain'),
        'required' => TRUE,
        'data_type' => 'String',
        'is_view' => 0,
      ],
      'admin_login' => [
        'type' => 'text',
        'title' => ts('Admin Login'),
        'required' => TRUE,
        'data_type' => 'String',
        'is_view' => 0,
      ],
      'admin_password' => [
        'type' => 'text',
        'title' => ts('Admin Password'),
        'required' => TRUE,
        'data_type' => 'String',
        'is_view' => 0,
      ],
      'company_logo' => [
        'type' => 'File',
        'title' => ts('Company Logo'),
        'required' => FALSE,
        'data_type' => 'File',
        'is_view' => 0,
      ],
      'server_id' => [
        'type' => 'text',
        'title' => ts('Server ID'),
        'required' => FALSE,
        'data_type' => 'String',
        'is_view' => 1,
      ],
      'server_ip' => [
        'type' => 'text',
        'title' => ts('Server IP'),
        'required' => FALSE,
        'data_type' => 'String',
        'is_view' => 1,
      ],
      'server_status' => [
        'type' => 'text',
        'title' => ts('Server Status'),
        'required' => FALSE,
        'data_type' => 'String',
        'is_view' => 1,
      ],
    ];

    return $siteInfoFields;
  }

  /**
   * @param $params
   *
   * @return array
   */
  public static function resolveSiteInfoParams($params) {
    $siteInfoParams = [];

    foreach (self::getSiteInfoFields() as $name => $field) {
      if (!empty($params[$name])) {
        $siteInfoParams[$name] = $params[$name];
      }
    }

    return $siteInfoParams;
  }

  public static function getSiteInfo($params, $limit, $offset = 0, $count = FALSE) {
    $query = CRM_Utils_SQL_Select::from(CRM_Mycivicrm_BAO_SiteInfo::getTableName() . ' si')
      ->join('ss', 'LEFT JOIN civicrm_site_server ss ON si.site_server_id = ss.id')
      ->join('hs', 'LEFT JOIN civicrm_handle_site hs ON si.id = hs.site_info_id')
      ->join('si','LEFT JOIN civicrm_membership cm ON si.membership_id = cm.id')
      ->join('cmt','LEFT JOIN civicrm_membership_type cmt ON cm.membership_type_id = cmt.id')
      ->join('cc','LEFT JOIN civicrm_contact cc ON cm.contact_id = cc.id');

    if (!empty($params['site_status'])) {
      $query->where('si.site_status = @site_status', ['site_status' => $params['site_status']]);
    }
    if (!empty($params['contact_id'])) {
      if (is_array($params['contact_id'])) {
        $query->where('cc.id IN (#contact_id)', ['contact_id' => $params['contact_id']]);
      } else {
        $query->where('cc.id = #contact_id', ['contact_id' => $params['contact_id']]);
      }
    }
    if (!empty($params['package'])) {
      if (is_array($params['package'])) {
        $query->where('si.package_name IN (@package)', ['package' => $params['package']]);
      } else {
        $query->where('si.package_name = @package', ['package' => $params['package']]);
      }
    }

    if ($limit) {
      $query->limit($limit, $offset);
    }

    if ($count) {
      $query->select('COUNT(si.id) as count');

      return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll()[0];
    }
    else {
      $query->select('
        si.id AS id,
        si.id AS site_info_id,
        cc.id AS contact_id,
        cm.end_date as end_date,
        cc.display_name AS display_name,
        cm.id AS membership_id,
        cmt.name AS membership_type,
        cmt.duration_unit AS duration_unit,
        cmt.duration_interval AS duration_interval,
        si.package_name AS package_name,
        si.website_domain AS website_domain,
        si.site_status as site_status,
        ss.server_id AS server_id,
        ss.server_ip AS server_ip,
        ss.server_status AS server_status,
        hs.id AS handle_site_id,
        hs.`status` AS handle_site_status
      ');

      return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll();
    }
  }

  /**
   * @param $contactId
   * @param $limit
   * @param int $offset
   * @param bool $count
   *
   * @return array
   * @throws \CRM_Core_Exception
   */
  public static function getDashbordSiteInfo($contactId, $limit, $offset = 0, $count = FALSE) {
    $query = CRM_Utils_SQL_Select::from(CRM_Mycivicrm_BAO_SiteInfo::getTableName() . ' si')
      ->join('ss', 'LEFT JOIN civicrm_site_server ss ON si.site_server_id = ss.id')
      ->join('si','LEFT JOIN civicrm_membership cm ON si.membership_id = cm.id')
      ->join('cmt','LEFT JOIN civicrm_membership_type cmt ON cm.membership_type_id = cmt.id')
      ->join('cc','LEFT JOIN civicrm_contact cc ON cm.contact_id = cc.id')
      ->where('cc.id = #contact_id', ['contact_id' => $contactId]);

    if ($limit) {
      $query->limit($limit, $offset);
    }

    if ($count) {
      $query->select('COUNT(cc.id) as count');

      return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll()[0];
    }
    else {
      $query->select('
        cm.id as membership_id,
        cc.id as contact_id,
        cm.end_date as end_date,
        si.package_name as package_name,
        si.id as site_info_id,
        si.company_name as company_name,
        si.website_domain as website_domain,
        ss.server_id as server_id,
        si.site_status as site_status
      ');
    }

    return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll();
  }

}
