<?php
use CRM_Mycivicrm_ExtensionUtil as E;

class CRM_Mycivicrm_Page_SiteInfo extends CRM_Core_Page {

  public function run() {
    // get the requested action
    $action = CRM_Utils_Request::retrieve('action', 'String',
      // default to 'browse'
      $this, FALSE, 'browse'
    );

    // assign vars to templates
    $this->assign('action', $action);
    $id = CRM_Utils_Request::retrieve('id', 'Positive',
      $this, FALSE, 0
    );

//    if ($action & CRM_Core_Action::VIEW) {
//      $this->view();
//    }
    // what actions to take ?
    if ($action & CRM_Core_Action::ADD) {
    }
    elseif ($action & CRM_Core_Action::UPDATE) {
    }
    elseif ($action & CRM_Core_Action::COPY) {
    }
    else {
      // finally browse the contribution pages
      $this->browse();

      CRM_Utils_System::setTitle(E::ts('SiteInfo'));
    }

    return parent::run();
  }

  /**
   * Browse all contribution pages.
   *
   * @param mixed $action
   *   Unused parameter.
   */
  public function browse($action = NULL) {
    Civi::resources()->addStyleFile('civicrm', 'css/searchForm.css', 1, 'html-header');

    $this->search();
    $params = [];

    if ($this->get('contact_id')) {
      $params['contact_id'] = \CRM_Utils_Array::explodePadded($this->get('contact_id'), ',');
      $this->assign('isSearch', 1);
    }

    if ($params['package'] = $this->get('package')) {
      $params['package'] = $this->get('package');
      $this->assign('isSearch', 1);
    }

    $pager = new CRM_Utils_Pager([
      'status' => ts('%%StatusMessage%%'),
      'rowCount' => $this->get(CRM_Utils_Pager::PAGE_ROWCOUNT) ?? Civi::settings()->get('default_pager_size'),
      'total' => CRM_Mycivicrm_SiteInfoHandler::getSiteInfo($params, 0, 0, TRUE)['count'],
    ]);
    $this->assign_by_ref('pager', $pager);

    list($offset, $rowCount) = $pager->getOffsetAndRowCount();
    $rows = CRM_Mycivicrm_SiteInfoHandler::getSiteInfo($params, $rowCount, $offset);

    foreach ($rows as &$row) {
      $row['contact_url'] = CRM_Utils_System::url('civicrm/contact/view', "reset=1&cid={$row['contact_id']}&selectedChild=member");
      $row['membership_url'] = CRM_Utils_System::url('civicrm/contact/view/membership', "reset=1&action=view&cid={$row['contact_id']}&id={$row['membership_id']}");
    }

    $this->assign('rows', $rows);
  }

  public function search() {
    if (isset($this->_action) & (CRM_Core_Action::ADD | CRM_Core_Action::UPDATE | CRM_Core_Action::DELETE)) {
      return;
    }

    $form = new CRM_Core_Controller_Simple('CRM_Mycivicrm_Form_SiteInfo', ts('Search Site Info'), CRM_Core_Action::ADD);
    $form->setEmbedded(TRUE);
    $form->setParent($this);
    $form->process();
    $form->run();
  }

}
