<?php

class CRM_Mycivicrm_BAO_SiteInfo extends CRM_Mycivicrm_DAO_SiteInfo {

  /**
   * Adds params to Site Info table.
   *
   * @param $params
   *
   * @return \CRM_Core_DAO
   */
  public static function add(&$params) {
    $entity = new CRM_Mycivicrm_DAO_SiteInfo();
    $entity->copyValues($params);

    return $entity->save();
  }

  /**
   * Creates new row in Mailchimp Contacts table.
   *
   * @param $params
   *
   * @return \CRM_Core_DAO
   */
  public static function &create(&$params) {
    $transaction = new self();

    if (!empty($params['id'])) {
      CRM_Utils_Hook::pre('edit', self::getEntityName(), $params['id'], $params);
    }
    else {
      CRM_Utils_Hook::pre('create', self::getEntityName(), NULL, $params);
    }

    $entityData = self::add($params);

    if (is_a($entityData, 'CRM_Core_Error')) {
      $transaction->rollback();

      return $entityData;
    }

    $transaction->commit();

    if (!empty($params['id'])) {
      CRM_Utils_Hook::post('edit', self::getEntityName(), $entityData->id, $entityData);
    }
    else {
      CRM_Utils_Hook::post('create', self::getEntityName(), $entityData->id, $entityData);
    }

    return $entityData;
  }

  /**
   * Deletes row.
   *
   * @param $id
   */
  public static function del($id) {
    $entity = new CRM_Mycivicrm_DAO_SiteInfo();
    $entity->id = $id;
    $params = [];

    if ($entity->find(TRUE)) {
      CRM_Utils_Hook::pre('delete', self::getEntityName(), $entity->id, $params);
      $entity->delete();
      CRM_Utils_Hook::post('delete', self::getEntityName(), $entity->id, $entity);
    }
  }

  /**
   * Builds query for receiving data.
   *
   * @param string $returnValue
   *
   * @return \CRM_Utils_SQL_Select
   */
  private static function buildSelectQuery($returnValue = 'rows') {
    $query = CRM_Utils_SQL_Select::from(CRM_Mycivicrm_DAO_SiteInfo::getTableName() . ' si')
      ->join('ss', 'LEFT JOIN civicrm_site_server ss ON si.site_server_id = ss.id');

    if ($returnValue == 'rows') {
      $query->select('
        si.id AS id,
        si.membership_id AS membership_id,
        si.contribution_id AS contribution_id,
        si.package_name AS package_name,
        si.company_name AS company_name,
        si.company_email AS company_email,
        si.website_domain AS website_domain,
        si.admin_login AS admin_login,
        si.admin_password AS admin_password,
        si.company_logo AS company_logo,
        si.site_status AS site_status,
        si.site_server_id AS site_server_id,
        ss.server_id AS server_id,
        ss.server_ip AS server_ip,
        ss.server_status AS server_status
      ');
    }
    else {
      if ($returnValue == 'count') {
        $query->select('COUNT(id)');
      }
    }

    return $query;
  }

  /**
   * Builds 'where' condition for query.
   *
   * @param $query
   * @param array $params
   *
   * @return mixed
   */
  private static function buildWhereQuery($query, $params = []) {
    if (!empty($params['id'])) {
      $query->where('id = #id', ['id' => $params['id']]);
    }

    if (!empty($params['membership_id'])) {
      $query->where('membership_id = #membership_id', ['membership_id' => $params['membership_id']]);
    }

    if (!empty($params['contribution_id'])) {
      $query->where('contribution_id = #contribution_id', ['contribution_id' => $params['contribution_id']]);
    }

    if (!empty($params['company_name'])) {
      $query->where('company_name = @company_name', ['company_name' => $params['company_name']]);
    }

    if (!empty($params['company_email'])) {
      $query->where('company_email = @company_email', ['company_email' => $params['company_email']]);
    }

    if (!empty($params['website_domain'])) {
      $query->where('website_domain = @website_domain', ['website_domain' => $params['website_domain']]);
    }

    if (!empty($params['site_server_id'])) {
      $query->where('site_server_id = @site_server_id', ['site_server_id' => $params['site_server_id']]);
    }

    if (!empty($params['site_status'])) {
      if (is_array($params['site_status'])) {
        $query->where('si.site_status IN (@site_status)', ['site_status' => $params['site_status']]);
      } else {
        $query->where('si.site_status = @site_status', ['site_status' => $params['site_status']]);
      }
    }

    return $query;
  }

  /**
   * Gets all data.
   *
   * @param array $params
   *
   * @return array
   */
  public static function getAll($params = []) {
    $query = self::buildWhereQuery(self::buildSelectQuery(), $params);

    if (!empty($params['limit']) && (int) $params['limit'] != 0) {
      $query->limit((int) $params['limit']);
    }

    return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll();
  }

}
