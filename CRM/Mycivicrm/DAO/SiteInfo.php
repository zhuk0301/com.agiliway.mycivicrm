<?php

class CRM_Mycivicrm_DAO_SiteInfo extends CRM_Core_DAO {

  /**
   * Static instance to hold the table name.
   *
   * @var string
   */
  static $_tableName = 'civicrm_site_info';

  /**
   * Static entity name.
   *
   * @var string
   */
  static $entityName = 'SiteInfo';

  /**
   * Should CiviCRM log any modifications to this table in the civicrm_log
   * table.
   *
   * @var boolean
   */
  static $_log = TRUE;

  /**
   * Unique id of current row.
   *
   * @var int
   */
  public $id;

  /**
   * CiviCRM membership id.
   *
   * @var int
   */
  public $membership_id;

  /**
   * CiviCRM contribution id.
   *
   * @var int
   */
  public $contribution_id;

  /**
   * Company Name.
   *
   * @var string
   */
  public $company_name;

  /**
   * Company Email.
   *
   * @var string
   */
  public $company_email;

  /**
   * Website Domain.
   *
   * @var string
   */
  public $website_domain;

  /**
   * Admin Login.
   *
   * @var string
   */
  public $admin_login;

  /**
   * Admin Password.
   *
   * @var string
   */
  public $admin_password;

  /**
   * Company Logo.
   *
   * @var string
   */
  public $company_logo;

  /**
   * Site Status.
   *
   * @var string
   */
  public $site_status;

  /**
   * Site Server ID.
   *
   * @var string
   */
  public $site_server_id;

  /**
   * Returns the names of this table.
   *
   * @return string
   */
  static function getTableName() {
    return self::$_tableName;
  }

  /**
   * Returns entity name.
   *
   * @return string
   */
  static function getEntityName() {
    return self::$entityName;
  }

  /**
   * Returns all the column names of this table.
   *
   * @return array
   */
  static function &fields() {
    if (!isset(Civi::$statics[__CLASS__]['fields'])) {
      Civi::$statics[__CLASS__]['fields'] = [
        'id' => [
          'name' => 'id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('id'),
          'description' => 'id',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'membership_id' => [
          'name' => 'membership_id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('CiviCRM Membership ID'),
          'description' => 'CiviCRM membership id',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.membership_id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'contribution_id' => [
          'name' => 'contribution_id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('CiviCRM Contribution ID'),
          'description' => 'CiviCRM contribution id',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.contribution_id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'package_name' => [
          'name' => 'package_name',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Package Name'),
          'description' => 'Package Name',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.package_mame',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'company_name' => [
          'name' => 'company_name',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Company Name'),
          'description' => 'Company Name',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.company_mame',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'company_email' => [
          'name' => 'company_email',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Company Email'),
          'description' => 'Company Email',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.company_email',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'website_domain' => [
          'name' => 'website_domain',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Website Domain'),
          'description' => 'Website Domain',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.website_domain',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'admin_login' => [
          'name' => 'admin_login',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Admin Login'),
          'description' => 'Admin Login',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.admin_login',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'admin_password' => [
          'name' => 'admin_password',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Admin Password'),
          'description' => 'Admin Password',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.admin_password',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'company_logo' => [
          'name' => 'company_logo',
          'type' => CRM_Utils_Type::T_TEXT,
          'title' => ts('Company Logo'),
          'description' => 'Company Logo',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.company_logo',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],

        'site_status' => [
          'name' => 'site_status',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Server Status'),
          'description' => 'Server Status',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.site_status',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
        'site_server_id' => [
          'name' => 'site_server_id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('SiteServer ID'),
          'description' => '',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.site_server_id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteInfo',
        ],
      ];

      CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'fields_callback', Civi::$statics[__CLASS__]['fields']);
    }

    return Civi::$statics[__CLASS__]['fields'];
  }

}
