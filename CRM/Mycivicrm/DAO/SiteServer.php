<?php

class CRM_Mycivicrm_DAO_SiteServer extends CRM_Core_DAO {

  /**
   * Static instance to hold the table name.
   *
   * @var string
   */
  static string $_tableName = 'civicrm_site_server';

  /**
   * Static entity name.
   *
   * @var string
   */
  static string $entityName = 'SiteServer';

  /**
   * Should CiviCRM log any modifications to this table in the civicrm_log
   * table.
   *
   * @var boolean
   */
  static bool $_log = TRUE;

  /**
   * Unique id of current row.
   *
   * @var int
   */
  public int $id;

  /**
   * Server ID.
   *
   * @var string
   */
  public string $server_id;

  /**
   * Server IP.
   *
   * @var string
   */
  public string $server_ip;

  /**
   * Server Status.
   *
   * @var string
   */
  public string $server_status;

  /**
   * Returns the names of this table.
   *
   * @return string
   */
  static function getTableName(): string {
    return self::$_tableName;
  }

  /**
   * Returns entity name.
   *
   * @return string
   */
  static function getEntityName(): string {
    return self::$entityName;
  }

  /**
   * Returns all the column names of this table.
   *
   * @return array
   */
  static function &fields(): array {
    if (!isset(Civi::$statics[__CLASS__]['fields'])) {
      Civi::$statics[__CLASS__]['fields'] = [
        'id' => [
          'name' => 'id',
          'type' => CRM_Utils_Type::T_INT,
          'title' => ts('id'),
          'description' => 'id',
          'required' => TRUE,
          'import' => TRUE,
          'where' => self::getTableName() . '.id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteServer',
        ],
        'server_id' => [
          'name' => 'server_id',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Server ID'),
          'description' => 'Server ID',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.server_id',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteServer',
        ],
        'server_ip' => [
          'name' => 'server_ip',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Server IP'),
          'description' => 'Server IP',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.server_ip',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteServer',
        ],
        'server_status' => [
          'name' => 'server_status',
          'type' => CRM_Utils_Type::T_STRING,
          'title' => ts('Server Status'),
          'description' => 'Server Status',
          'required' => FALSE,
          'import' => TRUE,
          'where' => self::getTableName() . '.server_status',
          'headerPattern' => '',
          'dataPattern' => '',
          'export' => TRUE,
          'table_name' => self::getTableName(),
          'entity' => self::getEntityName(),
          'bao' => 'CRM_Mycivicrm_BAO_SiteServer',
        ],
      ];

      CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'fields_callback', Civi::$statics[__CLASS__]['fields']);
    }

    return Civi::$statics[__CLASS__]['fields'];
  }

}
