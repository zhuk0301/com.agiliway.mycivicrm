<?php

class CRM_Mycivicrm_Form_Setting_Vultr extends CRM_Admin_Form_Setting {

  protected $_settings = [
    'vultr_api_key' => 'Vultr Setting',
    'vultr_region_id' => 'Vultr Setting',
    'vultr_plan_id' => 'Vultr Setting',
    'vultr_snapshot_id' => 'Vultr Setting',
    'vultr_label' => 'Vultr Setting',
    'vultr_os_id' => 'Vultr Setting',
  ];

  /**
   * Build the form object.
   */
  public function buildQuickForm() {
    CRM_Utils_System::setTitle(ts('Vultr Setting'));

    parent::buildQuickForm();
  }

}
