<?php

class CRM_Mycivicrm_Form_Setting_LimitServers extends CRM_Admin_Form_Setting {

  protected $_settings = [
    'limit_of_trail_servers' => 'Limit Servers',
    'limit_of_basic_servers' => 'Limit Servers',
    'limit_of_advanced_servers' => 'Limit Servers',
    'limit_of_custom_servers' => 'Limit Servers',
  ];

  /**
   * Build the form object.
   */
  public function buildQuickForm() {
    CRM_Utils_System::setTitle(ts('Limit Servers Setting'));

    parent::buildQuickForm();
  }

}
