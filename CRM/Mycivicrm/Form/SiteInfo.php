<?php

use CRM_Mycivicrm_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/quickform/
 */
class CRM_Mycivicrm_Form_SiteInfo extends CRM_Core_Form {

  public function buildQuickForm() {
    $this->add('text', 'title', ts('Find'), [
      'maxlength' => 255,
      'size' => 45,
      'style' => 'width: 90%',
    ]);
    $this->addEntityRef('contact_id', 'Contact', ['multiple' => TRUE]);
    $this->add('select', 'package', ts('Package'), [
      CRM_Mycivicrm_Package::TRIAL => CRM_Mycivicrm_Package::TRIAL,
      CRM_Mycivicrm_Package::BASIC => CRM_Mycivicrm_Package::BASIC,
      CRM_Mycivicrm_Package::ADVANCED => CRM_Mycivicrm_Package::ADVANCED,
      CRM_Mycivicrm_Package::CUSTOM => CRM_Mycivicrm_Package::CUSTOM
    ], FALSE, [
      'multiple' => TRUE,
      'class' => 'crm-select2 huge',
      'placeholder' => ts('- select Package -'),
    ]);

    $this->addButtons([
      [
        'type' => 'refresh',
        'name' => ts('Search'),
        'isDefault' => TRUE,
      ],
    ]);
  }

  public function postProcess() {
    $params = $this->controller->exportValues($this->_name);
    $parent = $this->controller->getParent();
    $parent->set('searchResult', 1);
    if (!empty($params)) {
      $fields = ['title', 'contact_id', 'package'];
      foreach ($fields as $field) {
        if (isset($params[$field]) && !CRM_Utils_System::isNull($params[$field])) {
          $parent->set($field, $params[$field]);
        } else {
          $parent->set($field, NULL);
        }
      }
    }
  }

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  public function getRenderableElementNames() {
    $elementNames = [];
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
  }

}
