<?php

class CRM_Mycivicrm_Server {

  /**
   * Get server param (id, ip)
   *
   * @param $subscriptionName
   *
   * @return array
   * @throws \Exception
   */
  public static function getServerParamOrCreateNewServer($subscriptionName) {
    $serverParam = self::getFreeVirtualServer($subscriptionName);

    if(empty($serverParam['server_id'])) {
      $server = new CRM_Mycivicrm_Server();
      $serverParam = $server->createNewServer();
    }

    return $serverParam;
  }

  /**
   * Get free server.
   *
   * @param $subscriptionName
   *
   * @return array
   * @throws Exception
   */
  public static function getFreeVirtualServer($subscriptionName) {
    switch ($subscriptionName) {
      case CRM_Mycivicrm_Package::TRIAL:
        $servers = self::getFreeTrialVirtualServer();
        break;
      case CRM_Mycivicrm_Package::BASIC:
        $servers = self::getFreeBasicVirtualServer();
        break;
      case CRM_Mycivicrm_Package::ADVANCED:
        $servers = self::getFreeAdvancedVirtualServer();
        break;
      case CRM_Mycivicrm_Package::CUSTOM:
        $servers = self::getFreeCustomVirtualServer();
        break;
      default:
        $servers = [];
    }

    $serverApi = new CRM_Mycivicrm_Vultr();

    foreach ($servers as $val) {
      $server = $serverApi->vultr->getServer($val['server_id']);

      if (!empty($server) && $serverApi->vultr->checkServerStatus($server)) {
        return [
          'site_server_id' => $val['site_server_id'],
          'server_id' => $server->id,
          'server_ip' => $server->main_ip,
        ];
      }
    }

    return [
      'site_server_id' => NULL,
      'server_id' => NULL,
      'server_ip' => NULL,
    ];
  }

  /**
   * Create New Server
   *
   * @return array (id, ip)
   */
  public function createNewServer() {
    $serverApi = new CRM_Mycivicrm_Vultr();
    $serverNew = $serverApi->vultr->createServer($serverApi->getCreateServerParams());
    $server = $serverApi->vultr->getServer($serverNew->id);

    if ($server) {
      return [
        'site_server_id' => NULL,
        'server_id' => $server->id,
        'server_ip' => $server->main_ip,
      ];
    }

    return [
      'site_server_id' => NULL,
      'server_id' => NULL,
      'server_ip' => NULL,
    ];
  }

  /**
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  private static function getFreeTrialVirtualServer() {
    $limit = (int) Civi::settings()->get('limit_of_trail_servers') ?: 10;

    return self::getServersByParams(CRM_Mycivicrm_Package::TRIAL , $limit);
  }

  /**
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  private static function getFreeBasicVirtualServer() {
    $limit = (int) Civi::settings()->get('limit_of_basic_servers') ?: 5;

    return self::getServersByParams(CRM_Mycivicrm_Package::BASIC , $limit);
  }

  /**
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  private static function getFreeAdvancedVirtualServer() {
    $limit = (int) Civi::settings()->get('limit_of_advanced_servers') ?: 1;

    return self::getServersByParams(CRM_Mycivicrm_Package::ADVANCED , $limit);
  }


  /**
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  private static function getFreeCustomVirtualServer() {
    $limit = (int) Civi::settings()->get('limit_of_custom_servers') ?: 1;

    return self::getServersByParams(CRM_Mycivicrm_Package::CUSTOM , $limit);
  }


  /**
   * @param $subscriptionName
   * @param $limit
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  private static function getServersByParams($subscriptionName, $limit) {
    $query = CRM_Utils_SQL_Select::from('civicrm_site_info ci')
      ->select('
        ss.id AS site_server_id,
        ss.server_id AS server_id,
        count(ci.id) AS cnt
      ')
      ->join('ss', 'LEFT JOIN civicrm_site_server ss ON ci.site_server_id = ss.id')
      ->where('ci.package_name = @1')
      ->where('ss.server_id is NOT NULL')
      ->groupBy('server_id')
      ->having('cnt < #2')
      ->param([
        1 => $subscriptionName,
        2 => $limit,
      ]);

    return $query->execute()->fetchAll();
  }
}
