<div class="crm-block crm-form-block crm-search-site-info-form-block">
  <table class="form-layout-compressed">
    <tr>
      <td>
        {$form.contact_id.label}
        <br/>
        {$form.contact_id.html}
      </td>
      <td>
        {$form.package.label}
        <br/>
        {$form.package.html}
      </td>
    </tr>
  </table>
  <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl"}</div>
</div>
