<div class="help">
    {ts}Help Text{/ts}
</div>
<div class="crm-block crm-form-block crm-limitservers-form-block">
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="top"}</div>
    <table class="form-layout">
        <tr class="crm-form-block-limitservers_limit_of_trail">
            <td class="label">{$form.limit_of_trail_servers.label}</td>
            <td>{$form.limit_of_trail_servers.html}<br />
        </tr>
        <tr class="crm-form-block-limitservers_limit_of_basic">
            <td class="label">{$form.limit_of_basic_servers.label}</td>
            <td>{$form.limit_of_basic_servers.html}<br />
        </tr>
        <tr class="crm-form-block-limitservers_limit_of_advanced">
            <td class="label">{$form.limit_of_advanced_servers.label}</td>
            <td>{$form.limit_of_advanced_servers.html}<br />
        </tr>
        <tr class="crm-form-block-limitservers_limit_of_custom">
            <td class="label">{$form.limit_of_custom_servers.label}</td>
            <td>{$form.limit_of_custom_servers.html}<br />
        </tr>
    </table>
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
    <div class="spacer"></div>
</div>
