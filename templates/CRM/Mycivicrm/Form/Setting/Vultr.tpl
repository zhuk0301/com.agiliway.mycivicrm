<div class="help">
    {ts}Help Text{/ts}
</div>
<div class="crm-block crm-form-block crm-vultr-form-block">
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="top"}</div>
    <table class="form-layout">
        <tr class="crm-form-block-vultr_api_key">
            <td class="label">{$form.vultr_api_key.label}</td>
            <td>{$form.vultr_api_key.html}<br />
        </tr>
        <tr class="crm-form-block-vultr_region_id">
            <td class="label">{$form.vultr_region_id.label}</td>
            <td>{$form.vultr_region_id.html}<br />
        </tr>
        <tr class="crm-form-block-vultr_plan_id">
            <td class="label">{$form.vultr_plan_id.label}</td>
            <td>{$form.vultr_plan_id.html}<br />
        </tr>
        <tr class="crm-form-block-vultr_snapshot_id">
            <td class="label">{$form.vultr_snapshot_id.label}</td>
            <td>{$form.vultr_snapshot_id.html}<br />
        </tr>
        <tr class="crm-form-block-vultr_label">
            <td class="label">{$form.vultr_label.label}</td>
            <td>{$form.vultr_label.html}<br />
        </tr>
        <tr class="crm-form-block-vultr_os_id">
            <td class="label">{$form.vultr_os_id.label}</td>
            <td>{$form.vultr_os_id.html}<br />
        </tr>
    </table>
    <div class="crm-submit-buttons">{include file="CRM/common/formButtons.tpl" location="bottom"}</div>
    <div class="spacer"></div>
</div>
