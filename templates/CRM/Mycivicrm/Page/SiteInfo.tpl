{include file="CRM/Mycivicrm/Form/SiteInfo.tpl"}

{if $rows}
  <div id="site_info">
    {strip}
      {include file="CRM/common/pager.tpl" location="top"}
      <table id="options" class="display">
        <thead>
        <tr>
          <th>{ts}Contact{/ts}</th>
          <th>{ts}Package{/ts}</th>
          <th>{ts}Site{/ts}</th>
          <th>{ts}Site Status{/ts}</th>
          <th>{ts}Site Status Process{/ts}</th>
          <th>{ts}Server ID{/ts}</th>
          <th>{ts}Server IP{/ts}</th>
          <th>{ts}Server Status{/ts}</th>
          <th></th>
        </tr>
        </thead>
          {foreach from=$rows item=row}
            <tr id="site-info-{$row.id}" class="crm-entity">
              <td><a href="{$row.contact_url}">{$row.display_name}</a></td>
              <td><a href="{$row.membership_url}">{$row.package_name}</a></td>
              <td>{$row.website_domain}</td>
              <td>{$row.site_status}</td>
              <td>{$row.handle_site_status}</td>
              <td>{$row.server_id}</td>
              <td>{$row.server_ip}</td>
              <td id="row_{$row.id}_status">{$row.server_status}</td>
            </tr>
          {/foreach}
      </table>
    {/strip}
    {include file="CRM/common/pager.tpl" location="bottom"}
  </div>
{else}
    {if $isSearch eq 1}
      <div class="status messages no-popup">
        <img src="{$config->resourceBase}i/Inform.gif" alt="{ts}status{/ts}"/>
          {ts}No sites match your search criteria{/ts}
        <div class="spacer"></div>
      </div>
    {else}
      <div class="messages status no-popup">
          {icon icon="fa-info-circle"}{/icon}
          {ts}No created sites{/ts}
      </div>
    {/if}
{/if}
