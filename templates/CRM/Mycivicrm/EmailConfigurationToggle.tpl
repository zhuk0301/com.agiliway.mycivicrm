{literal}
<script>
  CRM.$(function ($) {
    checkCheckBox();
    $('[data-crm-custom = "Site_information:Email_configuration"]').change(function() {
      checkCheckBox();
    });

    function checkCheckBox() {
      if (!$('[data-crm-custom = "Site_information:Email_configuration"]').is(':checked')) {
        $("#" + $("label:contains('Protocol')").parent().parent().attr("id")).css( "display", 'none');
        $("#" + $('[data-crm-custom = "Site_information:Port"]').parent().parent().attr("id")).css( "display", 'none');
        $("#" + $('[data-crm-custom = "Site_information:Security_mode"]').parent().parent().attr("id")).css( "display", 'none');
        $("#" + $('[data-crm-custom = "Site_information:Login"]').parent().parent().attr("id")).css( "display", 'none');
        $("#" + $('[data-crm-custom = "Site_information:Password"]').parent().parent().attr("id")).css( "display", 'none');
      } else {
        $("#" + $("label:contains('Protocol')").parent().parent().attr("id")).css( "display", 'block');
        $("#" + $('[data-crm-custom = "Site_information:Port"]').parent().parent().attr("id")).css( "display", 'block');
        $("#" + $('[data-crm-custom = "Site_information:Security_mode"]').parent().parent().attr("id")).css( "display", 'block');
        $("#" + $('[data-crm-custom = "Site_information:Login"]').parent().parent().attr("id")).css( "display", 'block');
        $("#" + $('[data-crm-custom = "Site_information:Password"]').parent().parent().attr("id")).css( "display", 'block');
      }
    }
  });
</script>
{/literal}
