<?php

class Vultr {

  /**
   * @var string
   */
  private $apiUrl = 'https://api.vultr.com/v2/';

  /**
   * @var string
   */
  private $apiKey;

  /**
   * Vultr constructor.
   *
   * @param $apiKey
   */
  public function __construct($apiKey) {
    $this->apiKey = $apiKey;
  }

  /**
   * @param $params
   *
   * @return object
   */
  public function createServer($params) {
    $server = $this->sendRequest('instances', 'POST', $params);

    return isset($server->instance) ? $server->instance : NULL;
  }

  /**
   * @return array
   */
  public function getServers() {
    $servers = $this->sendRequest('instances', 'GET');

    return isset($servers->instances) ? $servers->instances : [];
  }

  /**
   * @param $serverId
   *
   * @return object
   */
  public function getServer($serverId) {
    $server = $this->sendRequest('instances/' . $serverId, 'GET');

    return isset($server->instance) ? $server->instance : NULL;
  }

  /**
   * @param $server
   *
   * @return bool
   */
  public function checkServerStatus($server) {
    if (
      $server &&
      $server->status === 'active' &&
      $server->power_status === 'running' &&
      $server->server_status === 'ok'
    ) {
      return TRUE;
    }

    return FALSE;

  }

  /**
   * @param $serverId
   */
  public function startServer($serverId) {
    $this->sendRequest('instances/' . $serverId . '/start', 'POST');
  }

  /**
   * @param $serverId
   */
  public function rebootServer($serverId) {
    $this->sendRequest('instances/' . $serverId . '/reboot', 'POST');
  }

  /**
   * @param $serverId
   *
   * @return object
   */
  public function reinstallServer($serverId) {
    $server = $this->sendRequest('instances/' . $serverId . '/reinstall', 'POST');

    return isset($server->instance) ? $server->instance : NULL;
  }

  /**
   * @param $serverId
   */
  public function haltServer($serverId) {
    $this->sendRequest('instances/' . $serverId . '/halt', 'POST');
  }

  /**
   * @param $serverId
   */
  public function deleteServer($serverId) {
    $this->sendRequest('instances/' . $serverId, 'DELETE');
  }

  /**
   * @return array
   */
  public function getRegions() {
    $response = $this->sendRequest('regions', 'GET');

    return isset($response->regions) ? $response->regions : [];
  }

  /**
   * @return array
   */
  public function getPlans() {
    $response = $this->sendRequest('plans', 'GET');

    return isset($response->plans) ? $response->plans : [];
  }

  /**
   * @return array
   */
  public function getSnapshots() {
    $response = $this->sendRequest('snapshots', 'GET');

    return isset($response->snapshots) ? $response->snapshots : [];
  }

  /**
   * @return array
   */
  public function getOS() {
    $response = $this->sendRequest('os', 'GET');

    return isset($response->os) ? $response->os : [];
  }

  /**
   * @param $domain
   * @param $serverIp
   *
   * @return object
   */
  public function createDomain($domain, $serverIp) {
    $response = $this->sendRequest('domains', 'POST', [
      'domain' => $domain,
      'ip' => $serverIp,
    ]);

    return isset($response->domain) ? $response->domain : NULL;
  }

  /**
   * @param $domain
   * @param $name
   * @param $serverIp
   *
   * @return object
   */
  public function createDNSRecord($domain, $name, $serverIp) {
    $response = $this->sendRequest('domains/' . $domain . '/records', 'POST', [
      'name' => $name,
      'data' => $serverIp,
      'type' => 'A',
      'ttl' => 120,
    ]);

    return isset($response->record) ? $response->record : NULL;
  }

  /**
   * @param $url
   * @param $method
   * @param array $data
   *
   * @return mixed
   */
  private function sendRequest($url, $method, $data = []) {
    try {
      $curl = curl_init($this->apiUrl . $url);

      if ($curl === FALSE) {
        throw new \Exception('failed to initialize');
      }

      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
      curl_setopt($curl, CURLOPT_HTTPHEADER, [
        'Authorization: Bearer ' . $this->apiKey,
        'Content-Type: application/json',
      ]);

      $response = curl_exec($curl);

      if ($response === FALSE) {
        throw new \Exception(curl_error($curl), curl_errno($curl));
      }

      curl_close($curl);

      return json_decode($response);
    } catch (\Exception $exception) {
      trigger_error(
        sprintf(
          'Curl failed with error #%d: %s',
          $exception->getCode(), $exception->getMessage()
        ),
        E_USER_ERROR
      );

      return NULL;
    }
  }
}
