<?php

/**
 * This api is used for working with scheduled "cron" jobs.
 */

/**
 * Set server status to active
 *
 * @return array
 * @throws \CiviCRM_API3_Exception
 */
function civicrm_api3_job_mycivicrm_server_status(): array {
  $siteServer = CRM_Mycivicrm_BAO_SiteServer::getAll(['server_status' => 'installing',]);
  $serverApi = new CRM_Mycivicrm_Vultr();

  foreach ($siteServer as $value) {
    $server = $serverApi->vultr->getServer($value['server_id']);

    if (!empty($server) && $serverApi->vultr->checkServerStatus($server)) {
      $siteServerParams = [
        'id' => $value['id'],
        'server_status' => 'active',
        'server_ip' => $server->main_ip,
      ];
      CRM_Mycivicrm_BAO_SiteServer::create($siteServerParams);
    }
  }

  return civicrm_api3_create_success();
}

/**
 * Process new sites
 *
 * @throws CiviCRM_API3_Exception
 * @throws Exception
 */
function civicrm_api3_job_mycivicrm_process_new(): array {
  $siteInfo = CRM_Mycivicrm_BAO_SiteInfo::getAll(['site_status' => ['process_new', 'process_restore']]);

  foreach ($siteInfo as $value) {
    if (!CRM_Mycivicrm_Package::checkIsContributionCompleted($value['contribution_id'])) continue;
    if ($value['package_name'] == CRM_Mycivicrm_Package::TRIAL && CRM_Mycivicrm_Package::hasAlreadyTrial($value['membership_id'])) continue;

    if (empty($value['site_server_id'])) {
      $serverParam = CRM_Mycivicrm_Server::getServerParamOrCreateNewServer($value['package_name']);
      if (!$serverParam['site_server_id'] && !$serverParam['server_id']) continue;
      if (!$serverParam['site_server_id']) {
        $siteServerParam = [
          'server_id' => $serverParam['server_id'],
          'server_ip' => $serverParam['server_ip'],
          'server_status' => 'installing',
        ];
        $siteServerData = CRM_Mycivicrm_BAO_SiteServer::create($siteServerParam);
        $serverParam['site_server_id'] = $siteServerData->id;
      } else {$value['server_status'] = 'active';}
      $siteInfoParams = [
        'id' => $value['id'],
        'site_server_id' => $serverParam['site_server_id'],
      ];
      CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
    }

    if ($value['server_status'] === 'active') {
      if ($value['site_status'] == 'process_new') {
        $siteInfoParams = [
          'id' => $value['id'],
          'site_status' => 'pending',
        ];
        $siteInfoData = CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
        CRM_Mycivicrm_Package::insertHandleSite([
          'site_info_id' => $siteInfoData->id,
          'status' => 'pending',
        ]);
      }
      if ($value['site_status'] == 'process_restore') {
        $handleSite = CRM_Mycivicrm_Package::getHandleSiteBySiteInfoId($value['id']);
        CRM_Mycivicrm_Package::updateHandleSiteStatus($handleSite[0]['handle_site_id'], 'restore');
      }
    }
  }

  return civicrm_api3_create_success();
}

/**
 * Update site status after success set up
 *
 * @throws CiviCRM_API3_Exception
 */
function civicrm_api3_job_mycivicrm_site_status(): array {
  $siteInfo = CRM_Mycivicrm_SiteInfoHandler::getSiteInfo(['site_status' => 'active',], 0);
  foreach ($siteInfo as $value) {
    if (CRM_Utils_Date::overdue($value['end_date'])) {
      $siteInfoParams = [
        'id' => $value['site_info_id'],
        'site_status' => 'process_stop',
      ];
      CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
      CRM_Mycivicrm_Package::updateHandleSiteStatus($value['handle_site_id'], 'stop');
    }
  }

  foreach (['active', 'stopped'] as $status) {
    $siteInfo = CRM_Mycivicrm_Package::getHandleSiteByStatus($status);
    foreach ($siteInfo as $value) {
      $siteInfoParams = [
        'id' => $value['site_info_id'],
        'site_status' => $status,
      ];
      CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
    }
  }

  return civicrm_api3_create_success();
}

function civicrm_api3_job_mycivicrm_process_renew(): array {
  $siteInfo = CRM_Mycivicrm_SiteInfoHandler::getSiteInfo(['site_status' => ['process_renew']], 0);


  foreach ($siteInfo as $value) {
    if ($value['package_name'] != 'Trial') continue;

    $endDate = $value['end_date'];
    if (CRM_Utils_Date::overdue($value['end_date'])) {
      $endDate = NULL;
    }
    $date = new DateTime($endDate);
    $date->modify('+'.$value['duration_interval'].' ' . $value['duration_unit']);

    civicrm_api3('Membership', 'create', [
      'id' => $value['membership_id'],
      'end_date' => $date->format('Y-m-d'),
    ]);

    $siteInfoParams = [
      'id' => $value['site_info_id'],
    ];
    if ($value['handle_site_status'] == 'stopped') {
      CRM_Mycivicrm_Package::updateHandleSiteStatus($value['handle_site_id'], 'restore');
      $siteInfoParams['site_status'] = 'restore';
    } else {
      $siteInfoParams['site_status'] = 'active';
    }
    CRM_Mycivicrm_BAO_SiteInfo::create($siteInfoParams);
  }

  return civicrm_api3_create_success();
}