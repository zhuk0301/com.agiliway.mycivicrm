<?php

require_once 'mycivicrm.civix.php';
use CRM_Mycivicrm_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function mycivicrm_civicrm_config(&$config) {
  _mycivicrm_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function mycivicrm_civicrm_install() {
  _mycivicrm_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function mycivicrm_civicrm_postInstall() {
  _mycivicrm_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function mycivicrm_civicrm_uninstall() {
  _mycivicrm_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function mycivicrm_civicrm_enable() {
  _mycivicrm_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function mycivicrm_civicrm_disable() {
  _mycivicrm_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function mycivicrm_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _mycivicrm_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function mycivicrm_civicrm_entityTypes(&$entityTypes) {
  _mycivicrm_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function mycivicrm_civicrm_navigationMenu(&$menu) {
  _mycivicrm_civix_insert_navigation_menu($menu, 'Administer/', [
    'name' => ts('Vultr Setting'),
    'url' => 'civicrm/mycivicrm/setting/vultr',
    'permission' => 'administer CiviCRM',
    'operator' => NULL,
    'separator' => NULL,
  ]);
  _mycivicrm_civix_insert_navigation_menu($menu, 'Administer/', [
    'name' => ts('Limit Servers Setting'),
    'url' => 'civicrm/mycivicrm/setting/limit-servers',
    'permission' => 'administer CiviCRM',
    'operator' => NULL,
    'separator' => NULL,
  ]);
  _mycivicrm_civix_insert_navigation_menu($menu, 'Administer/', [
    'name' => ts('Site Info'),
    'url' => 'civicrm/mycivicrm/site-info',
    'permission' => 'administer CiviCRM',
    'operator' => NULL,
    'separator' => NULL,
  ]);
}

/**
 * Implements hook_civicrm_pre().
 *
 * @param $op
 * @param $objectName
 * @param $id
 * @param $params
 *
 * @throws \CRM_Core_Exception
 */
function mycivicrm_civicrm_pre($op, $objectName, $id, &$params) {
  CRM_Mycivicrm_Mycivicrm::pre($op, $objectName, $id, $params);
}

/**
 * Implements hook_civicrm_preProcess().
 *
 * @param $formName
 * @param $form
 *
 * @throws \CRM_Core_Exception
 */
function mycivicrm_civicrm_preProcess($formName, &$form) {
  CRM_Mycivicrm_SiteInfoHandler::preProcess($formName, $form);
}

/**
 * Implements hook_civicrm_postProcess().
 *
 * @param $formName
 * @param $form
 *
 * @throws \CRM_Core_Exception
 * @throws \CiviCRM_API3_Exception
 */
function mycivicrm_civicrm_postProcess($formName, $form) {
  CRM_Mycivicrm_Mycivicrm::postProcess($formName, $form);
  CRM_Mycivicrm_SiteInfoHandler::postProcess($formName, $form);
}

/**
 * Implements hook_civicrm_validateForm().
 *
 * @param string $formName
 * @param array $fields
 * @param array $files
 * @param CRM_Core_Form $form
 * @param array $errors
 */
function mycivicrm_civicrm_validateForm($formName, &$fields, &$files, &$form, &$errors) {
  CRM_Mycivicrm_Mycivicrm::processValidateForm($formName, $fields, $files, $form, $errors);
}

/**
 * Implements hook_civicrm_buildForm().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_buildForm/
 *
 * @param $formName
 * @param $form
 *
 * @throws \CRM_Core_Exception
 */
function mycivicrm_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contribute_Form_Contribution_Main') {
    CRM_Core_Region::instance('page-body')->add([
      'template' => E::path() . '/templates/CRM/Mycivicrm/EmailConfigurationToggle.tpl',
    ]);
  }

  CRM_Mycivicrm_SiteInfoHandler::buildForm($formName, $form);
}

/**
 * @param $IPNData
 *
 * @throws \CiviCRM_API3_Exception
 */
function mycivicrm_civicrm_postIPNProcess(&$IPNData) {
  $data =  json_decode(base64_decode($IPNData['data']), TRUE);
}
